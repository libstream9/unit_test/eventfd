#include <stream9/linux/eventfd/eventfd.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace lx { using namespace stream9::linux; }

BOOST_AUTO_TEST_SUITE(eventfd_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(success_)
        {
            lx::eventfd efd;

            BOOST_TEST(efd.path() == "anon_inode:[eventfd]");
        }

        BOOST_AUTO_TEST_CASE(failure_)
        {

            auto make_eventfd = []{ lx::eventfd efd { 0, 100 }; };

            BOOST_CHECK_EXCEPTION(
                make_eventfd(),
                stream9::error,
                [&](auto&& e) {
                    BOOST_TEST(e.why() == lx::errc::einval);
                    return true;
                });
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_SUITE(read_)

        BOOST_AUTO_TEST_CASE(blocking_1_)
        {
            lx::eventfd efd { 100 };

            auto i = efd.read().or_throw();

            BOOST_TEST(i == 100);
        }

        BOOST_AUTO_TEST_CASE(non_blocking_)
        {
            lx::eventfd efd { 0, EFD_NONBLOCK };

            auto o_i = efd.read();

            BOOST_CHECK(o_i == EAGAIN);
        }

    BOOST_AUTO_TEST_SUITE_END() // read_

    BOOST_AUTO_TEST_SUITE(write_)

        BOOST_AUTO_TEST_CASE(blocking_)
        {
            lx::eventfd efd;

            efd.write(100);

            auto i = efd.read().or_throw();

            BOOST_TEST(i == 100);
        }

        BOOST_AUTO_TEST_CASE(non_blocking_)
        {
            lx::eventfd efd { 0, EFD_NONBLOCK };

            auto o1 = efd.write(UINT64_MAX - 1);
            BOOST_CHECK(!o1.has_error());

            auto o2 = efd.write(1);
            BOOST_CHECK(o2 == EAGAIN);
        }

    BOOST_AUTO_TEST_SUITE_END() // write_

BOOST_AUTO_TEST_SUITE_END() // eventfd_

} // namespace testing
